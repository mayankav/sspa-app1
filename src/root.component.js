import React from "react";
import Hello from "./components/Hello";

export default function Root(props) {
  const styles = {
    wrapper: {
      border: "1px solid #2445b1",
      margin: "20px auto 20px auto",
      paddingBottom: "20px",
      textAlign: "center",
      backgroundColor: "#8d9dcf",
      width: "500px",
    },
  };

  return (
    <div style={styles.wrapper}>
      <Hello />
    </div>
  );
}
