import React, { useState, useEffect } from "react";

const Hello = (props) => {
  const styles = {
    counter: {
      color: "yellow",
    },
  };

  const [localCounter, setLocalCounter] = useState(window.sspa_demo.counter);
  useEffect(() => {
    window.addEventListener("onIncrement", () => {
      setLocalCounter(window.sspa_demo.counter);
    });
    window.addEventListener("onDecrement", () => {
      setLocalCounter(window.sspa_demo.counter);
    });
  }, []);

  return (
    <>
      <h1>This is a React app.</h1>
      <div>
        The counter value is{" "}
        <span style={styles.counter}> {localCounter} </span>
      </div>
    </>
  );
};

export default Hello;
